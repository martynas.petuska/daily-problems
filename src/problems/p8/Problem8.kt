package problems.p8

/**
 * A unival tree (which stands for "universal value") is a tree where all nodes under it have the same value.
 * Given the root to a binary tree, count the number of unival subtrees.
 * For example, the following tree has 5 unival subtrees:
 *
 *    0
 *   / \
 *  1   0
 *     / \
 *    1   0
 *   / \
 *  1   1
 *
 */

class Node(val value: Int, val left: Node?, val right: Node?)

fun main() {
    val tree = Node(
            0,
            Node(1, null, null), Node(
            0,
            Node(1, Node(1, null, null), Node(1, null, null)),
            Node(0, null, null)
    )
    )
    assert(countUnival(tree) == 5)
}

fun countUnival(root: Node): Int {
    TODO()
}