package problems.p10

import java.util.*

/**
 * Implement a job scheduler which takes in a function f and an integer n, and calls f after n milliseconds.
 */

fun main() {
    val offset = 5 * 1000L
    val start = Date().time
    var end = 0L
    callInMs({
        end = Date().time
    }, offset)
    assert(end - start == offset)
}

fun callInMs(f: () -> Unit, delayMS: Long) {
    TODO()
}