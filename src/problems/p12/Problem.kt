package problems.p12

/**
 * There exists a staircase with N steps, and you can climb up either 1 or 2 steps at a time.
 * Given N, write a function that returns the number of unique ways you can climb the staircase.
 * The order of the steps matters.
 *
 * For example, if N is 4, then there are 5 unique ways:
 *
 * 1, 1, 1, 1
 * 2, 1, 1
 * 1, 2, 1
 * 1, 1, 2
 * 2, 2
 *
 * What if, instead of being able to climb 1 or 2 steps at a time,
 * you could climb any number from a set of positive integers X?
 * For example, if X = {1, 3, 5}, you could climb 1, 3, or 5 steps at a time.
 *
 * So for N = 5 & X = {1, 3, 5}:
 *
 * 1, 1, 1, 1, 1
 * 3, 1, 1
 * 1, 3, 1
 * 1, 1, 3
 * 5
 *
 * And for N = 6 & X = {2, 3, 4}:
 *
 * 2, 2, 2
 * 3, 3
 * 2, 4
 * 4, 2
 */

fun main() {
    assert(climbWays1or2(4) == 5)
    assert(climbWaysX(5, listOf(1, 3, 5)) == 5)
    assert(climbWaysX(6, listOf(2, 3, 4)) == 4)
}

fun climbWays1or2(n: Int): Int {
    TODO()
}

fun climbWaysX(n: Int, x: List<Int>): Int {
    TODO()
}