package problems.p5

/**
 * cons(a, b) constructs a pair, and car(pair) and cdr(pair) returns the first and last element of that pair.
 * For example, car(cons(3, 4)) returns 3, and cdr(cons(3, 4)) returns 4.
 *
 * Given this implementation of problem.cons:
 *
 * def cons(a, b):
 *   def pair(f):
 *     return f(a, b)
 *   return pair
 *
 * Implement car and cdr.
 */
fun main() {
    assert(car(cons(3, 4)) == 3)
    assert(cdr(cons(3, 4)) == 4)
}

private fun cons(a: Int, b: Int): ((Int, Int) -> Unit) -> Unit {
    return fun(f: (Int, Int) -> Unit) {
        f(a, b)
    }
}

private fun car(pair: ((Int, Int) -> Unit) -> Unit): Int? {
    TODO()
}

private fun cdr(pair: ((Int, Int) -> Unit) -> Unit): Int? {
    TODO()
}