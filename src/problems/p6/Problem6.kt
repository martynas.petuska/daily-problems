package problems.p6

/**
 * An XOR linked list is a more memory efficient doubly linked list.
 * Instead of each node holding next and prev fields, it holds a field named both, which is an XOR of the next node and the previous node.
 * Implement an XOR linked list; it has an add(element) which adds the element to the end, and a get(index) which returns the node at index.
 *
 * If using a language that has no pointers (such as Python),
 * you can assume you have access to get_pointer and dereference_pointer functions that converts between nodes and memory addresses.
 */
fun main() {
    val xorList = XORList<String>()
    xorList.add("one")
    xorList.add("two")
    xorList.add("three")
    assert(xorList.get(0) == "one")
    assert(xorList.get(1) == "two")
    assert(xorList.get(2) == "three")
}

open class XORList<T> {
    open fun add(element: T) {
        TODO()
    }

    open fun get(index: Int): T {
        TODO()
    }
}