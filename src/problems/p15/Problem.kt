package problems.p15

import java.util.stream.Stream

/**
 * Given a stream of elements too large to store in memory,
 * pick a random element from the stream with uniform probability.
 */

fun main() {
    pickRandom(Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9))
}

fun <T> pickRandom(stream: Stream<T>): T {
    TODO()
}