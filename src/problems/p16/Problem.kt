package problems.p16

/**
 * You run an e-commerce website and want to record the last N order ids in a log.
 * Implement a data structure to accomplish this, with the following API:
 *   record(order_id): adds the order_id to the log
 *   get_last(i): gets the ith last element from the log. i is guaranteed to be smaller than or equal to N.
 *
 *   You should be as efficient with time and space as possible.
 */

fun main() {
    TODO("check(LogImpl())")
}

fun check(log: Log<Long>) {
    for (i in 1..10) {
        log.record(i.toLong())
    }
    for (i in 10 downTo 1) {
        assert(log.getLast(i) == 11L - i)
    }
}

interface Log<T> {
    fun record(id: T)
    fun getLast(i: Int): Long?
}