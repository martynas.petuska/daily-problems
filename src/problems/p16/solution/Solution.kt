package problems.p16.solution

import problems.p16.Log
import problems.p16.check

/**
 * You run an e-commerce website and want to record the last N order ids in a log.
 * Implement a data structure to accomplish this, with the following API:
 *   record(order_id): adds the order_id to the log
 *   get_last(i): gets the ith last element from the log. i is guaranteed to be smaller than or equal to N.
 *
 *   You should be as efficient with time and space as possible.
 */

fun main() {
    check(OrderLog())
}

class OrderLog : Log<Long> {
    private val tail = LookbackNode(null, 0L)
    override fun record(id: Long) {
        tail.previous = LookbackNode(tail.previous, id)
    }

    override fun getLast(i: Int): Long? {
        return if (i > 0) {
            var node = tail
            for (idx in 0 until i) {
                node = node.previous!!
            }
            node.value
        } else {
            null
        }
    }

    private class LookbackNode<T>(var previous: LookbackNode<T>?, val value: T)
}