package problems.p14

/**
 * The area of a circle is defined as πr^2. Estimate π to 3 decimal places using a Monte Carlo method.
 *
 * Hint: The basic equation of a circle is x2 + y2 = r2.
 */

fun main() {
    val tests = 100
    var fails = 0
    repeat(tests) {
        if (estimatePI(10000) > 3.100 && estimatePI(1000) < 3.180
                && estimatePI(100000) > 3.120 && estimatePI(1000) < 3.160) {
            fails++
        }
    }
    assert(fails.toDouble() / tests < 0.5)
}

fun estimatePI(n: Int): Double {
    TODO()
}