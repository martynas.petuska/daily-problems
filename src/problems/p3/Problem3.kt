package problems.p3

/**
 * Given the root to a binary tree, implement problem.serialize(root), which serializes the tree into a string, and problem.deserialize(s),
 * which deserializes the string back into the tree.
 *
 * For example, given the following problem.Node class
 *
 * class problem.Node:
 *   def __init__(self, val, left=None, right=None):
 *     self.val = val
 *     self.left = left
 *     self.right = right
 *
 * The following test should pass:
 *
 * node = problem.Node("root", problem.Node("left", problem.Node("left.left")), problem.Node("right"))
 * assert problem.deserialize(problem.serialize(node)).left.left.val == "left.left"
 */
fun main() {
    val node =
            Node("root", Node("left", Node("left.left")), Node("right"))
    assert(deserialize(serialize(node))?.left?.left?.value == "left.left")
}

data class Node(val value: String, val left: Node? = null, val right: Node? = null)

private fun serialize(node: Node?): String {
    TODO()
}

private fun deserialize(node: String): Node? {
    TODO()
}