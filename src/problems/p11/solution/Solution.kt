package problems.p11.solution

/**
 * Implement an autocomplete system. That is, given a query string s and a set of all possible query strings,
 * return all strings in the set that have s as a prefix.
 * For example, given the query string de and the set of strings [dog, deer, deal], return [deer, deal].
 *
 * Hint: Try preprocessing the dictionary into a more efficient data structure to speed up queries.
 */

fun main() {
    val set = setOf("dog", "deer", "deal")
    assert(autocomplete("de", set) == listOf("deer", "deal"))
}

fun autocomplete(query: String, dictionary: Set<String>): List<String> {
    val processed = preprocess(dictionary)
    var current: Map<Char, Any?> = processed
    var done = false
    query.forEach {
        if (current[it] == null) {
            done = true
        } else if (!done) {
            current = current[it] as Map<Char, Any?>
        }
    }
    return flattenToStrings(query, current)
}

fun preprocess(dictionary: Set<String>): Map<Char, Any?> {
    var processed = mutableMapOf<Char, Any?>()
    dictionary.forEach {
        processed = flattenToMap(it, processed)
    }
    return processed.toMap()
}

fun flattenToMap(value: String, target: MutableMap<Char, Any?>): MutableMap<Char, Any?> {
    if (value.length > 1) {
        if (target[value[0]] == null) {
            target[value[0]] = flattenToMap(value.substring(1), mutableMapOf())
        } else if (target[value[0]] is MutableMap<*, *>?) {
            target[value[0]] = flattenToMap(value.substring(1), target[value[0]] as MutableMap<Char, Any?>)
        }
    } else {
        target[value[0]] = null
    }
    return target
}

fun flattenToStrings(prefix: String, dictionary: Map<Char, Any?>): List<String> {
    val tmp = mutableListOf<String>()
    dictionary.forEach { key, value ->
        if (value != null) {
            tmp.addAll(flattenToStrings("$prefix$key", value as Map<Char, Any?>))
        } else {
            tmp.add("$prefix$key")
        }
    }
    return tmp
}