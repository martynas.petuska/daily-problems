package problems.p11

/**
 * Implement an autocomplete system. That is, given a query string s and a set of all possible query strings,
 * return all strings in the set that have s as a prefix.
 * For example, given the query string de and the set of strings [dog, deer, deal], return [deer, deal].
 *
 * Hint: Try preprocessing the dictionary into a more efficient data structure to speed up queries.
 */

fun main() {
    val set = setOf("dog", "deer", "deal")
    assert(autocomplete("de", set) == listOf("deer", "deal"))
}

fun autocomplete(query: String, dictionary: Set<String>): List<String> {
    TODO()
}