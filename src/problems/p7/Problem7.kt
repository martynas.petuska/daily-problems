package problems.p7

/**
 * Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, count the number of ways it can be decoded.
 * For example, the message '111' would give 3, since it could be decoded as 'aaa', 'ka', and 'ak'.
 *
 * You can assume that the messages are decodable. For example, '001' is not allowed.
 *
 */
val mapping = mapOf(
        1 to "a", 2 to "b", 3 to "c", 4 to "d", 5 to "e",
        6 to "f", 7 to "g", 8 to "h", 9 to "i", 10 to "j",
        11 to "k", 12 to "l", 13 to "m", 14 to "n", 15 to "o",
        16 to "p", 17 to "q", 18 to "r", 19 to "s", 20 to "t",
        21 to "u", 22 to "v", 23 to "w", 24 to "x", 25 to "y", 26 to "z"
)

fun main() {
    val message = "111"
    assert(checkDecodeWays("111") == 3)
}

fun checkDecodeWays(message: String): Int {
    TODO()
}