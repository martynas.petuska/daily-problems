package problems.p13

/**
 * Given an integer k and a string s, find the length of the longest substring that contains at most k distinct characters.
 *
 * For example, given s = "abcba" and k = 2, the longest substring with k distinct characters is "bcb".
 */

fun main() {
    assert(longestSubstring("abcba", 2) == "bcb")
}

fun longestSubstring(s: String, k: Int): String {
    TODO()
}